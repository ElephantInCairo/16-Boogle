package body pparaula is
	use pila_char;
	
	procedure b_paraula (p : out paraula) is
	begin
		buida(p);
	end b_paraula;
	
	procedure assigna (a : in paraula; b : out paraula) is
		it : pl_iterador;
		c  : character;
	begin
		buida(b);
		primer(it);
		while es_valid(b, it) loop
			obt(b, it, c); seg(it);
			empila(b, c);
		end loop;
	end assigna;
	
	procedure append (p : in out paraula; c : in character) is
	begin
		empila(p, c);
	end append;
	
	--iteradors
	procedure primer (it : out p_iterador) is
	begin
		primer( pl_iterador(it) );
	end primer;
	
	procedure seg (it : in out p_iterador) is
	begin
		seg( pl_iterador(it) );
	end seg;
	
	procedure obt (p : in paraula; it : in p_iterador; c : out character) is
	begin
		obt(p, pl_iterador(it), c);
	end obt;
	
	function es_valid (p : in paraula; it : in p_iterador) return boolean is
	begin
		return es_valid(p, pl_iterador(it));
	end es_valid;

end pparaula;