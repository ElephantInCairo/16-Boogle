package body d_pila_esp is

    procedure buida (s : out pila) is
        top : index renames s.top;
    begin
        top := 0;
    end buida;

    procedure empila (s : in out pila; x : in item) is
        top : index     renames s.top;
        m   : mem_space renames s.m;
    begin
        if top = index(max) then raise desbordament; end if;
		top := top + 1; m(top) := x; --assigna(x, m(top));
    end empila;

    procedure cim (s : in pila; x : out item) is
        top : index     renames s.top;
        m   : mem_space renames s.m;
    begin
        if top = 0 then raise mal_us; end if;
        x := m(top); --assigna(m(top), x);
    end cim;

    procedure desempila (s : in out pila) is
        top : index renames s.top;
    begin
        if top = 0 then raise mal_us; end if;
        top := top - 1;
    end desempila;

    function es_buida (s : in pila) return boolean is
        top : index renames s.top;
    begin
        return top = 0;
    end es_buida;
	
	function hi_es (s : in pila; x : in item) return boolean is
		it : pl_iterador;
		y  : item;
	begin
		primer(it);
		while es_valid(s, it) loop
			obt(s, it, y); seg(it);
			if y = x then return true; end if;
		end loop;
		return false;
	end hi_es;

	--pl_iterador
	procedure primer (it : out pl_iterador) is
	begin
		it := 1;
	end primer;
	
	procedure seg (it : in out pl_iterador) is
	begin
		it := it + 1;
	end seg;
	
	procedure obt (p : in pila; it : in pl_iterador; x : out item) is
        m   : mem_space renames p.m;
		top : index renames p.top;
		i : index := index(it);
	begin
		if i > top then raise mal_us; end if;
		x := m(i);
	end obt;
	
	function es_valid(p : in pila; it : in pl_iterador) return boolean is
		top : index renames p.top;
		i : index := index(it);
	begin
		return i <= top;
	end es_valid;
	
end d_pila_esp;