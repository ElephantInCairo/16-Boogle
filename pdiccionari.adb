with ada.text_io; use ada.text_io;

package body pdiccionari is

	mk : constant character := '.';

	--part privada
	procedure buida (d : out diccionari) is
		pa : pnode renames d.pa;
	begin
		pa := null;
	end buida;
	
	procedure insereix_nivell (pa : in out pnode; pn : out pnode; x : in character) is
		pp, pi : pnode;
	begin
		pi := pa; pp := null;
		while pi /= null and then pi.x /= x loop
			pp := pi;
			if x < pi.x then pi := pi.pe;
			else			 pi := pi.pd;
			end if;
		end loop;
		if pi = null then
			pi := new node'(x, null, null, null);
			if pa = null   then pa := pi;
			elsif x < pp.x then pp.pe := pi;
			elsif x > pp.x then pp.pd := pi;
			end if;
		end if;
		pn := pi;
	end insereix_nivell;
	
	procedure obt_fill (p0 : in out pnode; x : in character) is
	begin
		while p0 /= null and then p0.x /= x loop
			if x < p0.x then p0 := p0.pe;
			else			 p0 := p0.pd;
			end if;
		end loop;
	end obt_fill;

	--part pública
	procedure b_diccionari(fnom : in string; d : out diccionari) is
		n : pnode;
		f : file_type;
		c : character;
	begin
		buida(d);
		open(f, in_file, fnom);
		while not end_of_file(f) loop
			if not end_of_line(f) then
				get(f, c);
				insereix_nivell(d.pa, n, c);
			end if;
			while not end_of_line(f) loop
				get(f, c);
				insereix_nivell(n.pseg, n, c);
			end loop;
			insereix_nivell(n.pseg, n, mk);
			if not end_of_file(f) then skip_line(f); end if;
		end loop;
		close(f);
	end b_diccionari;

	procedure cons_prefixe (d : in diccionari; p : in paraula; es_prefixe, es_paraula : out boolean) is
		pa : pnode := d.pa;
		itp : p_iterador;
		x : character;
	begin
		primer(itp); es_prefixe := true;
		while es_valid(p, itp) and es_prefixe loop
			obt(p, itp, x); seg(itp);
			obt_fill(pa, x);
			if pa = null then es_prefixe := false;
			else 			  pa := pa.pseg;
			end if;
		end loop;
		obt_fill(pa, mk);
		es_paraula := es_prefixe and pa /= null;
	end cons_prefixe;	

end pdiccionari;