with d_pila_esp;
with pparaula;

generic
	with package meu_pparaula is new pparaula(<>);
	mida: natural := 4;

package pboggle is
	use meu_pparaula;
	
	type tauler is private;
	procedure b_tauler        (t :    out tauler);
	procedure imprimeix_tauler(t : in     tauler);
	
	type t_iterador is limited private;
	procedure primer  (it :    out t_iterador);
	procedure obt     (t : in tauler; it : in     t_iterador; p : out paraula);
	function es_valid (it : in     t_iterador) return boolean;
	procedure seg     (it : in out t_iterador; es_prefixe : in boolean);
	
private
	subtype t_clau is natural range 1 .. mida;
	type tauler is array (t_clau, t_clau) of character;
	
	type t_direccio is (n, ne, e, se, s, so, o, no);
	package pila_dir is new d_pila_esp(t_direccio, 150); use pila_dir;
	type t_iterador is record
		x, y : t_clau;
		pd : pila;
		ev : boolean;
	end record;
	
end pboggle;