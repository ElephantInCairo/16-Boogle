with Ada.Text_IO, Ada.Characters.Latin_1, Ada.Characters.Handling;
use  Ada.Text_IO, Ada.Characters.Latin_1, Ada.Characters.Handling;
with d_pila_esp;

generic
	mida : positive := 30;
	
package pparaula is

	type paraula is limited private;
	procedure b_paraula(p :    out paraula);
	procedure append   (p : in out paraula; c :  in character);
	procedure assigna  (a : in     paraula; b : out paraula);
	--function es_buida  (p : in     paraula) return boolean;
	--function "="       (a, b : in  paraula) return boolean;
	
	type p_iterador is private;
	procedure primer (it :    out p_iterador);
	procedure seg    (it : in out p_iterador);
	procedure obt    (p : in paraula; it : in p_iterador; c : out character);
	function es_valid(p : in paraula; it : in p_iterador) return boolean;

private
	package pila_char is new d_pila_esp(character, mida);
	type paraula    is new pila_char.pila;
	type p_iterador is new pila_char.pl_iterador;

end pparaula;