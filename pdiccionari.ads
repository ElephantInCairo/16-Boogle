with pparaula;

generic
	with package meu_pparaula is new pparaula(<>);

package pdiccionari is
	use meu_pparaula;
	type diccionari is limited private;
	procedure b_diccionari (fnom : in string; d : out diccionari);
	procedure cons_prefixe (d : in diccionari; p : in paraula; es_prefixe, es_paraula : out boolean);
	
private
	type node;
	type pnode is access node;
	type node is record
		x : character;
		pe, pd, pseg : pnode;
	end record;
	type diccionari is record
		pa : pnode;
	end record;
	
end pdiccionari;