with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO;      use Ada.Text_IO;
with pparaula, pdiccionari, pboggle, d_pila_esp;

procedure main is
	package m_pparaula is new pparaula(16);
	package m_dicci    is new pdiccionari(m_pparaula);
	package m_pboggle  is new pboggle(m_pparaula, 10);
	package m_pila     is new d_pila_esp(character, 500);
	use m_pparaula, m_dicci, m_pboggle, m_pila;
	
	arg_error : exception;
	
	d : diccionari;
	t : tauler;
	--r : pila;
	
	procedure interseccio(t : in tauler; d : in diccionari) is
		p   : paraula;
		itt : t_iterador;
		es_paraula, es_prefixe : boolean;
		
		procedure posa (r : in out pila; p : in paraula) is
			it : p_iterador;
			c  : character;
		begin
			primer(it);
			while es_valid(p, it) loop
				obt(p, it, c); seg(it);
				empila(r, c);
			end loop;
			empila(r, '.');
		end posa;
		
		procedure mostra (p : in paraula) is
			it : p_iterador;
			c  : character;
		begin
			primer(it);
			while es_valid(p, it) loop
				obt(p, it, c); seg(it);
				put(c);
			end loop;
			new_line;
		end mostra;
		
	begin
		--buida(r);
		primer(itt);
		while es_valid(itt) loop
			obt(t, itt, p);
			cons_prefixe(d, p, es_prefixe, es_paraula);
			if es_paraula then mostra(p); end if;
			--if es_paraula then posa(r, p); end if;	
			seg(itt, es_prefixe);
		end loop;
	end interseccio;
	
begin
	if argument_count /= 1 then raise arg_error; end if;
	b_diccionari(argument(1), d);
	b_tauler(t);
	imprimeix_tauler(t);
	interseccio(t, d);

exception
	when arg_error => put("./main.exe dictionary.txt");
end Main;