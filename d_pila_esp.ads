generic
    type item is private;
	--with procedure assigna(a : in item; x : out item);
    max : natural := 50;

package d_pila_esp is
    pragma pure;
	
    type pila is limited private;
    procedure buida     (s :    out pila);
    procedure desempila (s : in out pila);
    procedure empila    (s : in out pila; x :  in item);
    procedure cim       (s : in     pila; x : out item);
    function es_buida   (s : in     pila) return boolean;
	function hi_es      (s : in     pila; x : in item) return boolean;
    mal_us       : exception;
    desbordament : exception;
	
	type pl_iterador is private;
	procedure primer (it :    out pl_iterador);
	procedure seg    (it : in out pl_iterador);
	procedure obt    (p : in pila; it : in pl_iterador; x : out item);
	function es_valid(p : in pila; it : in pl_iterador) return boolean;

private
    type index is new natural range 0 .. max;
    type mem_space is array (index range 1 .. index(max)) of item;
    type pila is record
        m   : mem_space;
        top : index;
    end record;
	
	type pl_iterador is new natural;

end d_pila_esp;
