with RandGen;      use RandGen;
with ada.Text_IO;  use Ada.Text_IO;

package body pboggle is

	subtype majuscules is character range 'A' .. 'Z';

	procedure b_tauler(t : out tauler) is
		idx : Integer;
	begin
		for i in Integer range t_clau'RANGE loop
			for j in Integer range t_clau'RANGE loop
				idx := generate_random_number (24) + 65; --generam lletres aleatories
				t(i,j):= majuscules'Val(idx);
			end loop;
		end loop;
	end b_tauler;

	procedure imprimeix_tauler(t: in tauler) is
	begin
		for i in reverse Integer range t_clau'RANGE loop
			for j in reverse Integer range t_clau'RANGE loop
				put(t(i,j));
			end loop;
			new_line;
		end loop;
	end imprimeix_tauler;
	
	--iterador fila/columna: mateix que tipus pila
	procedure pila_primer (it : out t_clau) is
	begin
		it := t_clau'First;
	end pila_primer;
	
	function es_pila_valid (itk : in t_clau) return boolean is
	begin
		return itk /= t_clau'Last;
	end es_pila_valid;
	
	procedure pila_seg (itk : in out t_clau) is
	begin
		itk := t_clau'Succ(itk);
	end pila_seg;

	--iterador direccions: mateix que tipus arbre preordre
	procedure direccio_primer (it : out pila) is
	begin
		buida(it);
	end direccio_primer;

	function es_direccio_valid (pd : in pila) return boolean is
		--per a cert nivell n, es valid si pot iterar sobre aquell nivell
		--nivell => paraules amb certa longitud n o menor formades pels caràcters
		--  resultants dels moviments de l'iterador.
		--és correcte que pila buida retorna fals, ja que si no existeix una
		-- paraula que començi per certa lletra (x,y) s'ha d'avançar a la seg
		-- casella.
		itd : pl_iterador;
		dir : t_direccio;
	begin
		primer(itd);
		while es_valid(pd, itd) loop
			obt(pd, itd, dir); seg(itd);
			if dir /= t_direccio'Last then return true; end if;
		end loop;
		return false;
	end es_direccio_valid;
	
	procedure direccio_seg (pd : in out pila) is
		--prec: es_direccio_valid;
		dir : t_direccio;
	begin
		cim(pd, dir);
		while dir = t_direccio'Last loop
			desempila(pd);
			cim(pd, dir);
		end loop;
		desempila(pd);
		empila(pd, t_direccio'Succ(dir));
	end direccio_seg;
	
	--iterador boggle: funcions d'ajuda
	procedure seg_casella (x, y : in out t_clau; d : in t_direccio; es_valid : out boolean) is
	begin
		case d is
			when n =>
				es_valid := x /= t_clau'Last;
				if es_valid then x := t_clau'Succ(x); end if;
			when s =>
				es_valid := x /= t_clau'First;
				if es_valid then x := t_clau'Pred(x); end if;
			when e =>
				es_valid := y /= t_clau'Last;
				if es_valid then y := t_clau'Succ(y); end if;
			when o =>
				es_valid := y /= t_clau'First;
				if es_valid then y := t_clau'Pred(y); end if;
			when ne =>
				es_valid := x /= t_clau'Last and y /= t_clau'Last;
				if es_valid then x := t_clau'Succ(x); y := t_clau'Succ(y); end if;
			when no =>
				es_valid := x /= t_clau'Last and y /= t_clau'First;
				if es_valid then x := t_clau'Succ(x); y := t_clau'Pred(y); end if;
			when se =>
				es_valid := x /= t_clau'First and y /= t_clau'Last;
				if es_valid then x := t_clau'Pred(x); y := t_clau'Succ(y); end if;
			when so =>
				es_valid := x /= t_clau'First and y /= t_clau'First;
				if es_valid then x := t_clau'Pred(x); y := t_clau'Pred(y); end if;
		end case;
	end seg_casella;
	
	--iterador boggle: part pública
	procedure primer (it : out t_iterador) is
		p1 : t_clau renames it.x;
		p2 : t_clau renames it.y;
		pd : pila renames it.pd;
		ev : boolean renames it.ev;
	begin
		pila_primer(p1);
		pila_primer(p2);
		direccio_primer(pd);
		ev := true;
	end primer;

	function es_valid (it : in t_iterador) return boolean is
		ev : boolean renames it.ev;
	begin
		return ev;
	end es_valid;

	procedure seg (it : in out t_iterador; es_prefixe : in boolean) is
		ev : boolean renames it.ev;
		
		procedure seg0 (it : in out t_iterador; es_prefixe : in boolean) is
			pd : pila    renames it.pd;
			p1 : t_clau  renames it.x;
			p2 : t_clau  renames it.y;
			ev : boolean renames it.ev;
		begin
			if    es_prefixe            then empila(pd, t_direccio'First); 
			elsif es_direccio_valid(pd) then direccio_seg(pd);
			else
				direccio_primer(pd);
				if    es_pila_valid(p2) then pila_seg(p2);
				elsif es_pila_valid(p1) then pila_seg(p1); pila_primer(p2);
				else ev := false;
				end if;
			end if;
		end seg0;
		
		function es_coherent (it : in t_iterador) return boolean is
			ev : boolean renames it.ev;
			pd : pila renames it.pd;
			itd : pl_iterador;
			dir : t_direccio;
			package vcaselles is new d_pila_esp(natural);
			cjt : vcaselles.pila;
			es_dins_tauler : boolean;
			x : t_clau := it.x;
			y : t_clau := it.y;

			function to_nat (x, y : in t_clau) return natural is
			begin
				return x * mida + y;
			end to_nat;
			
		begin
			if not ev then return true; end if;
			vcaselles.buida(cjt);
			primer(itd); vcaselles.empila(cjt, to_nat(x, y));
			while es_valid(pd, itd) loop
				obt(pd, itd, dir); seg(itd);
				seg_casella(x, y, dir, es_dins_tauler);
				if not es_dins_tauler or else vcaselles.hi_es(cjt, to_nat(x, y)) then return false; end if;
				vcaselles.empila(cjt, to_nat(x, y));
			end loop;
			return true;
		end es_coherent;
		
	begin
		if not ev then raise mal_us; end if;
		seg0(it, es_prefixe);
		while not es_coherent(it) loop
			seg0(it, false);
		end loop;
	end seg;

	procedure obt (t : in tauler; it : in t_iterador; p : out paraula) is
		pd : pila    renames it.pd;
		x : t_clau := it.x;
		y : t_clau := it.y;
		itd : pl_iterador;
		dir : t_direccio;
		z : boolean;
	begin
		b_paraula(p);
		append(p, t(x, y));
		primer(itd);
		while es_valid(pd, itd) loop
			obt(pd, itd, dir); seg(itd);
			seg_casella(x, y, dir, z);
			append(p, t(x, y));
		end loop;	
	end obt;
	
end pboggle;